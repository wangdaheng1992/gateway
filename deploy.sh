#!/usr/bin/env bash

CONTAINER_NAME=personal-blog-gateway
DOCKER_HOST="docker -H tcp://134.175.133.115:2375"

if  [ "$($DOCKER_HOST images -aq $CONTAINER_NAME)" ]; then
    echo "delete exist image"
    $DOCKER_HOST rmi -f $CONTAINER_NAME
fi


$DOCKER_HOST build -t $CONTAINER_NAME .

if [ "$($DOCKER_HOST ps -aq -f name=$CONTAINER_NAME)" ]; then
    if [ "$($DOCKER_HOST ps -q -f name=$CONTAINER_NAME)" ]; then
        echo "current docker container is running, stop first"
        $DOCKER_HOST stop $CONTAINER_NAME
    fi
    echo "personal-blog existed,delete first"
    $DOCKER_HOST rm $CONTAINER_NAME
fi

$DOCKER_HOST run -d -e SPRING_PROFILES_ACTIVE=$1 \
--restart always --name $CONTAINER_NAME -p 9999:9999 $CONTAINER_NAME \



