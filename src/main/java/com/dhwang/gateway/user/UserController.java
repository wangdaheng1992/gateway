package com.dhwang.gateway.user;

import com.dhwang.gateway.auth.SecurityUser;
import com.dhwang.gateway.util.SecurityUserUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static com.dhwang.gateway.constant.Constant.User.YTPOKJI_API_TOEKN;

@RequestMapping(value = "/api")
@RestController
public class UserController {

    @PostMapping("/login")
    public UserInfo login(HttpServletRequest request) {
        SecurityUser securityUser = SecurityUserUtil.getCurrentUser();
        request.getSession().setAttribute(YTPOKJI_API_TOEKN, securityUser.getId());
        UserInfo userInfo = new UserInfo(securityUser.getUserName(), securityUser.getName());
        return userInfo;
    }
}
