package com.dhwang.gateway.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {

    private String id;

    private String userName;

    private String name;

    public UserInfo(String userName, String name) {
        this.userName = userName;
        this.name = name;
    }
}
