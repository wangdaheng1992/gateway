package com.dhwang.gateway.client;

import com.dhwang.gateway.user.UserInfo;
import com.dhwang.gateway.user.dto.UserDTO;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "ytpokji", url = "${ytpokji.server.url}")
public interface YtpokjiApiClient {

    @PostMapping(value = "/api/users", consumes = "application/json")
    UserInfo getUserInfo(@RequestBody UserDTO userDTO);

}
