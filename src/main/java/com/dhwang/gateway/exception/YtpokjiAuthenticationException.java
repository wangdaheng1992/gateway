package com.dhwang.gateway.exception;


import org.springframework.security.core.AuthenticationException;

public class YtpokjiAuthenticationException extends AuthenticationException {

    public YtpokjiAuthenticationException(String msg, Throwable t) {
        super(msg, t);
    }
}
