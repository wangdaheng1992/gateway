package com.dhwang.gateway.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.Protocol;

@Configuration
@EnableRedisHttpSession
public class RedisFactoryConfiguration {

    @Value("${redis.host.url}")
    private String redisHost;

    @Bean
    @ConditionalOnProperty(name = "default.redis", havingValue = "true")
    @Qualifier("connectionFactory")
    public JedisConnectionFactory defaultJedisConnectionFactory(){
        return new JedisConnectionFactory(
                new JedisShardInfo(redisHost, Protocol.DEFAULT_PORT));
    }

    @Bean
    @Qualifier("connectionFactory")
    @ConditionalOnProperty(name = "default.redis", havingValue = "false")
//    @ConditionalOnMissingBean 和它上面的注解用一个就可以
    public JedisConnectionFactory jedisConnectionFactory(){
        return new JedisConnectionFactory(
                new JedisShardInfo(redisHost, 6380));
    }
}
