package com.dhwang.gateway.auth;

import com.dhwang.gateway.exception.YtpokjiAuthenticationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginAuthenticationFilter extends OncePerRequestFilter {

    private AuthenticationManager authenticationManager;
    private HttpStatusEntryPoint authenticationEntryPoint = new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED);

    public LoginAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (request.getMethod().equals("POST") && request.getRequestURI().equals("/api/login")) {
            //正常访问api login请求时需要执行的操作
            final LoginAuthenticationToken token = new ObjectMapper().
                    readValue(request.getInputStream(), LoginAuthenticationToken.class);
            Authentication authentication;
            try{
                authentication = authenticationManager.authenticate(token);
            }catch (Exception e){
                doAfterAuthenticateFail(request, response, new YtpokjiAuthenticationException("failed", e));
                return;
            }
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        filterChain.doFilter(request, response);
    }

    private void doAfterAuthenticateFail(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        logger.error("authenticate failed", failed);
        SecurityContextHolder.clearContext();
        authenticationEntryPoint.commence(request, response, failed);
    }
}
