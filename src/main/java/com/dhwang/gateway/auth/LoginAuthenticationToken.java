package com.dhwang.gateway.auth;

import com.dhwang.gateway.user.UserInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Data
@EqualsAndHashCode(callSuper=false)
public class LoginAuthenticationToken extends AbstractAuthenticationToken {

    private String userName;

    private String password;

    private String id;

    private String name;

    public LoginAuthenticationToken(String id, String userName, String name) {
        super(null);
        this.id = id;
        this.name = name;
        this.userName = userName;
        setAuthenticated(true);
    }

    public LoginAuthenticationToken(UserInfo userInfo) {
        super(null);
        this.id = userInfo.getId();
        this.name = userInfo.getName();
        this.userName = userInfo.getUserName();
        setAuthenticated(true);
    }

    public LoginAuthenticationToken() {
        super(null);
    }

    @Override
    public Object getCredentials() {
        return password;
    }

    @Override
    public Object getPrincipal() {
        return new SecurityUser(id, userName ,name);
    }


}
