package com.dhwang.gateway.auth;

import com.dhwang.gateway.client.YtpokjiApiClient;
import com.dhwang.gateway.user.UserInfo;
import com.dhwang.gateway.user.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class LoginAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private YtpokjiApiClient apiClient;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        log.info("authentication is {}", authentication);
        LoginAuthenticationToken token = (LoginAuthenticationToken) authentication;
        UserInfo userInfo = apiClient.getUserInfo(new UserDTO(token.getUserName(), token.getPassword()));
        return new LoginAuthenticationToken(userInfo);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return LoginAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
