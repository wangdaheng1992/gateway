package com.dhwang.gateway.auth;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SecurityUser {

    private String id;

    private String userName;

    private String name;
}
