package com.dhwang.gateway.util;

import com.dhwang.gateway.auth.SecurityUser;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUserUtil {

    private SecurityUserUtil(){

    }

    public static SecurityUser getCurrentUser(){
        SecurityUser currentUser =
                (SecurityUser) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal();

        return currentUser;
    }

    public static String getCurrentUserId(){
        SecurityUser currentUser = getCurrentUser();
        return currentUser.getId();
    }
}
