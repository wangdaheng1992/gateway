package com.dhwang.gateway.filter;

import com.dhwang.gateway.util.SecurityUserUtil;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.dhwang.gateway.constant.Constant.User.YTPOKJI_API_TOEKN;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;

@Slf4j
@Component
public class MyZuulPreFilter extends ZuulFilter{
    @Override
    public String filterType() {
        return PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        log.info("-----Log print::The filter excute before call api by gateway-----");
        RequestContext ctx = RequestContext.getCurrentContext();
        ctx.getZuulRequestHeaders().put(YTPOKJI_API_TOEKN, SecurityUserUtil.getCurrentUserId());
        return null;
    }
}
