#!/usr/bin/env bash

CONTAINER_NAME=fundsWatcher-redis
if [ ! "$(docker ps -q -f name=$CONTAINER_NAME)" ]; then
    if [ "$(docker ps -aq -f status=exited -f name=$CONTAINER_NAME)" ]; then
        # cleanup
        docker rm $CONTAINER_NAME
    fi
    # run your container
    docker run -d --restart always --name $CONTAINER_NAME \
    -p 6379:6379 redis:alpine
fi