FROM openjdk:8-jre-alpine
MAINTAINER wang daheng "dhwang@thoughtworks.com"
COPY /build/libs/gateway-1.0-SNAPSHOT.war /app/gateway-1.0-SNAPSHOT.war
ENTRYPOINT ["java"]
CMD ["-jar", "./app/gateway-1.0-SNAPSHOT.war"]
EXPOSE 9999
